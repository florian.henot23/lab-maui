﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

//using Newtonsoft.Json;

namespace PartsClient.Data
{
    public static class PartsManager
    {
        // TODO: Add fields for BaseAddress, Url, and authorizationKey
        static readonly string BaseAddress = "http://10.0.2.2:5210";
        static readonly string Url = $"{BaseAddress}/api";

        static HttpClient client;

        static string authKey;

        private static async Task<HttpClient> GetClient()
        {
            if (client is not null) return client;

            client = new HttpClient();

            if (string.IsNullOrEmpty(authKey))
            {
                authKey = Preferences.Get("authKey", string.Empty);

                if (string.IsNullOrEmpty(authKey))
                {
                    authKey = await client.GetFromJsonAsync<string>($"{Url}/login");
                    Preferences.Set("authKey", authKey);
                }
            }

            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Add("Authorization", authKey);

            return client;
        }

        public static async Task<IEnumerable<Part>> GetAll()
        {
            if (Connectivity.Current.NetworkAccess != NetworkAccess.Internet) return new List<Part>();
            
            var client = await GetClient();

            return await client.GetFromJsonAsync<IEnumerable<Part>>($"{Url}/parts");
        }

        public static async Task<Part> Add(string partName, string supplier, string partType)
        {
            if (Connectivity.Current.NetworkAccess != NetworkAccess.Internet) return null;

            var part = new Part()
            {
                PartName = partName,
                Suppliers = new List<string>(new[] { supplier }),
                PartID = string.Empty,
                PartType = partType,
                PartAvailableDate = DateTime.Now.Date
            };

            var msg = new HttpRequestMessage(HttpMethod.Post, $"{Url}/parts");
            msg.Content = JsonContent.Create(part);

            var client = await GetClient();

            var response = await client.SendAsync(msg);
            response.EnsureSuccessStatusCode();

            var itemJson = await response.Content.ReadAsStringAsync();
            var insertedPart = JsonSerializer.Deserialize<Part>(itemJson);

            return insertedPart;
        }

        public static async Task Update(Part part)
        {
            if (Connectivity.Current.NetworkAccess != NetworkAccess.Internet) return;

            var msg = new HttpRequestMessage(HttpMethod.Put, $"{Url}/parts/{part.PartID}");
            msg.Content = JsonContent.Create(part);

            var client = await GetClient();

            var response = await client.SendAsync(msg);
            response.EnsureSuccessStatusCode();
        }

        public static async Task Delete(string partID)
        {
            if (Connectivity.Current.NetworkAccess != NetworkAccess.Internet) return;

            var msg = new HttpRequestMessage(HttpMethod.Delete, $"{Url}/parts/{partID}");

            var client = await GetClient();

            var response = await client.SendAsync(msg);
            response.EnsureSuccessStatusCode();
        }
    }
}
