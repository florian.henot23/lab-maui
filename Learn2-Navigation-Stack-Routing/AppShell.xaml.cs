﻿using Learn2_Navigation_Stack_Routing.Pages;

namespace Learn2_Navigation_Stack_Routing
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            //URI absolue (pas nécessaire étant dans la classe Shell, mais plus sûr) et liaison hiérarchique à DataDisplay
            Routing.RegisterRoute("//base/DataDisplay/Details", typeof(DataDisplayDetails));
        }
    }
}
