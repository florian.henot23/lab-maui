namespace Learn2_Navigation_Stack_Routing.Pages;

public partial class DataDisplay : ContentPage
{
	public DataDisplay()
	{
		InitializeComponent();

        // URI relative vers DataDisplayDetails car route = //base/DataDisplay selon AppShell
		btnTable.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=La table de la salle � manger");
        btnBureau.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=Un bureau en bois custom");
        btnBouteille.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=Une bouteille d'eau gazeuze");
        btnSouris.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=La souris gamer de Florian");
    }
}