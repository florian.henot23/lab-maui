namespace Learn2_Navigation_Stack_Routing.Pages;

// R�cup�ration des param�tres de l'URI - Nom du param�tre sensible � la casse
[QueryProperty(nameof(Objet), "objet")]
public partial class DataDisplayDetails : ContentPage
{
    private string? objet;

	// Propri�t� r�ceptrice du param�tre de requ�te
    public string? Objet 
	{ 
		get => objet;
		set
		{
			objet = value;
			lblValueObjet.Text = objet;
		} 
	}

    public DataDisplayDetails()
	{
		InitializeComponent();
	}
}