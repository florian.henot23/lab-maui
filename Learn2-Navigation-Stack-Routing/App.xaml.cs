﻿namespace Learn2_Navigation_Stack_Routing
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();
        }
    }
}
