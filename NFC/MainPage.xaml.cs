﻿using System.Text;

namespace NFC;

public partial class MainPage : ContentPage
{
    readonly INfcService _nfcService;

    public MainPage(INfcService nfcService)
    {
        InitializeComponent();

        _nfcService = nfcService;

        btnSend.Clicked += BtnSend_Clicked;
    }

    protected async override void OnAppearing()
    {
        base.OnAppearing();

        if (await _nfcService.OpenNfcSettingsAsync())
        {
            //_nfcService.EnableForegroundDispatch();
        }
    }

    protected override void OnDisappearing()
    {
        base.OnDisappearing();

        //_nfcService.DisableForegroundDispatch();
    }

    private async void BtnSend_Clicked(object? sender, EventArgs e)
    {
        var data = Encoding.UTF8.GetBytes(edtData.Text);

        await _nfcService.SendAsync(data);
    }
}
