﻿using Android.App;
using Android.Content;
using Android.Nfc;
using Android.Nfc.Tech;
using Android.OS;
using Android.Provider;
using Android.Util;
using Microsoft.Extensions.Logging;
using System.Xml;
using Application = Microsoft.Maui.Controls.Application;

namespace NFC;

class NfcService : INfcService, IDisposable
{
    readonly MainActivity? _mainActivity;
    readonly Lazy<NfcAdapter?> _lazyAdapter;
    readonly PendingIntent? _pendingIntent;
    readonly IntentFilter[] _intentFilters;
    readonly string[][]? _techList;

    NfcAdapter? Adapter => _lazyAdapter.Value;

    public static Tag? DetectedTag { get; set; }

    public NfcService()
    {
        _mainActivity = (MainActivity?)Platform.CurrentActivity;

        _lazyAdapter = new(() => NfcAdapter.GetDefaultAdapter(_mainActivity));

        _intentFilters = [
            new(NfcAdapter.ActionTagDiscovered),
            new(NfcAdapter.ActionNdefDiscovered),
            new(NfcAdapter.ActionTechDiscovered)
         ];
        foreach (var intentFilter in _intentFilters) intentFilter.AddCategory(Intent.CategoryDefault);

        _techList = GetNfcTechFilters();

        var intent = new Intent(_mainActivity, _mainActivity?.Class)
            .AddFlags(ActivityFlags.SingleTop);
        var pendingIntentFlags = Build.VERSION.SdkInt >= BuildVersionCodes.S ?
            PendingIntentFlags.Mutable :
            0;
        _pendingIntent = PendingIntent.GetActivity(_mainActivity, 0, intent, pendingIntentFlags);

        Platform.ActivityStateChanged += Platform_ActivityStateChanged;

        Log.Debug("NfcService", "Service NFC créé");
    }

    private void Platform_ActivityStateChanged(object? sender, ActivityStateChangedEventArgs e)
    {
        switch (e.State)
        {
            case ActivityState.Resumed:
                EnableForegroundDispatch();
                break;
            case ActivityState.Paused:
                DisableForegroundDispatch();
                break;
            default:
                break;
        }

        Log.Debug("NfcService", "Changement état activité : {0}", e.State);
    }

    public void EnableForegroundDispatch()
    {
        Adapter?.EnableForegroundDispatch(_mainActivity, _pendingIntent, _intentFilters, _techList);
    }

    public void DisableForegroundDispatch()
    {
        Adapter?.DisableForegroundDispatch(_mainActivity);
    }

    public async Task SendAsync(byte[] bytes)
    {
        Ndef? ndef = null;

        try
        {
            DetectedTag ??= await GetDetectedTagAsync();

            ndef = Ndef.Get(DetectedTag);
            if (ndef is null) return;

            if (!ndef.IsWritable)
            {
                await Application.Current!.MainPage!.DisplayAlert("Erreur", "Le tag est en lecture seule", "Ok");
                return;
            }

            if (!ndef.IsConnected) await ndef.ConnectAsync();

            await WriteToTagAsync(ndef, bytes);
        }
        catch (IOException)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "Une erreur s'est produite lors de la transmission des données, qui peut être due au fait que l'appareil a été éloigné du tag.", "Ok");
        }
        catch (Exception ex)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", ex.Message, "Ok");
        }
        finally
        {
            if (ndef?.IsConnected ?? false) ndef.Close();

            DetectedTag = null;
        }
    }

    public async Task<bool> OpenNfcSettingsAsync()
    {
        if (Adapter is null)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "NFC non supporté", "Ok");
            return false;
        }

        if (!Adapter.IsEnabled)
        {
            await Application.Current!.MainPage!.DisplayAlert("Désactivé", "NFC désactivé", "Ok");

            var intent = new Intent(Settings.ActionNfcSettings);

            _mainActivity?.StartActivity(intent);
        }

        return true;
    }

    private static string[][]? GetNfcTechFilters()
    {
        List<List<string>> techList = [];
        int currentList = -1;

        using var reader = Platform.CurrentActivity?.Resources?.GetXml(Resource.Xml.nfc_tech_filter);

        if (reader is null) return null;

        reader.MoveToContent();

        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    if (reader.Name == "tech-list")
                    {
                        techList.Add([]);
                        currentList++;
                    }
                    break;
                case XmlNodeType.Text:
                    techList[currentList].Add(reader.Value);
                    break;
                default:
                    break;
            }
        }

        return techList.Select(x => x.ToArray()).ToArray();
    }

    private async Task<Tag?> GetDetectedTagAsync()
    {
        if (_mainActivity is null) return null;

        _mainActivity.NfcTag = new TaskCompletionSource<Tag>();

        var tagDetectionTask = await Task.WhenAny(_mainActivity.NfcTag.Task);

        return await tagDetectionTask;
    }

    private static async Task WriteToTagAsync(Ndef ndef, byte[] bytes)
    {
        var ndefRecord = new NdefRecord(NdefRecord.TnfWellKnown, NdefRecord.RtdText?.ToArray(), [], bytes);
        var ndefMessage = new NdefMessage([ndefRecord]);

        await ndef.WriteNdefMessageAsync(ndefMessage);

        await Application.Current!.MainPage!.DisplayAlert("Tag NFC", "Ecriture réussie", "Ok");
    }

    public void Dispose()
    {
        Platform.ActivityStateChanged -= Platform_ActivityStateChanged;

        Log.Debug("NfcService", "Service NFC supprimé");
    }
}
