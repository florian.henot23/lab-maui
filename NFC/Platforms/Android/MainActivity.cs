﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Nfc;
using Android.OS;
using ANfcService = NFC.NfcService;

namespace NFC;

[MetaData(NfcAdapter.ActionTechDiscovered, Resource = "@xml/nfc_tech_filter")]
[IntentFilter([NfcAdapter.ActionTechDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[IntentFilter([NfcAdapter.ActionNdefDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[IntentFilter([NfcAdapter.ActionTagDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
public class MainActivity : MauiAppCompatActivity
{
    public TaskCompletionSource<Tag>? NfcTag { get; set; }

    protected override void OnNewIntent(Intent? intent)
    {
        base.OnNewIntent(intent);

        switch (intent?.Action)
        {
            case NfcAdapter.ActionTagDiscovered:
            case NfcAdapter.ActionNdefDiscovered:
            case NfcAdapter.ActionTechDiscovered:
                ReadNfcIntent(intent);
                break;
            default:
                break;
        }
    }

    private void ReadNfcIntent(Intent intent)
    {
        Tag? tag;

        if (Build.VERSION.SdkInt >= BuildVersionCodes.Tiramisu)
        {
            var javaType = Java.Lang.Class.FromType(typeof(Tag));

            // Compatibilité entre les différentes versions (pas de check à réaliser et pas d'avertissement)
            //tag = (Tag?)IntentCompat.GetParcelableExtra(intent, NfcAdapter.ExtraTag, javaType);
#pragma warning disable CA1416
            // Avertissement car disponible uniquement depuis API 33
            tag = (Tag?)intent.GetParcelableExtra(NfcAdapter.ExtraTag, javaType);
#pragma warning restore CA1416
        }
        else
        {
#pragma warning disable CA1422
            tag = (Tag?)intent.GetParcelableExtra(NfcAdapter.ExtraTag);
#pragma warning restore CA1422
        }

        if (tag is null) return;

        var isSuccess = NfcTag?.TrySetResult(tag);
        if (!isSuccess ?? true) ANfcService.DetectedTag = tag;
    }
}
