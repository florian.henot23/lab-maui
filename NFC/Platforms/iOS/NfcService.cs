﻿using CoreNFC;
using UIKit;

namespace NFC;

class NfcService : INfcService
{
    public void DisableForegroundDispatch()
    {
        throw new NotImplementedException();
    }

    public void EnableForegroundDispatch()
    {
        throw new NotImplementedException();
    }

    public async Task SendAsync(byte[] bytes)
    {
        var isNfcAvailable = UIDevice.CurrentDevice.CheckSystemVersion(11, 0);
        if (!(isNfcAvailable && NFCNdefReaderSession.ReadingAvailable))
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "NFC non supporté", "Ok");
            return;
        }

        await MainThread.InvokeOnMainThreadAsync(async () =>
        {
            try
            {
                var sessionDelegate = new NfcSessionDelegate(bytes);
                var session = new NFCNdefReaderSession(sessionDelegate, null, true);

                session.BeginSession();
            }
            catch
            {
                await Application.Current!.MainPage!.DisplayAlert("Erreur", "Impossible de créer une session NFC", "Ok");
            }
        });
    }
}
