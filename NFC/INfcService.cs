﻿namespace NFC;

 public interface INfcService
{
    /// <summary>
    /// Active recherche NFC
    /// </summary>
    void EnableForegroundDispatch();

    /// <summary>
    /// Désactive recherche NFC
    /// </summary>
    void DisableForegroundDispatch();

    /// <summary>
    /// Envoyer données via NFC
    /// </summary>
    /// <param name="bytes">Charge utile</param>
    Task SendAsync(byte[] bytes);

    /// <summary>
    /// Open NFC settings
    /// </summary>
    /// <returns>True if NFC is available on device</returns>
    Task<bool> OpenNfcSettingsAsync() => Task.FromResult(true);
}
