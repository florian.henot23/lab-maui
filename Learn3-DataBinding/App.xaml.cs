﻿
namespace Learn3_DataBinding
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new AppShell();

            Routing.RegisterRoute(nameof(Learn3_DataBinding.MainPage), typeof(Learn3_DataBinding.MainPage));
        }

        protected override Window CreateWindow(IActivationState? activationState)
        {
            Window window = base.CreateWindow(activationState);
#if WINDOWS
            window.Width = 500;
            window.Height = 300;
#endif
            return window;
        }
    }
}
