﻿using Learn3_DataBinding.Models;
using System.Globalization;

namespace Learn3_DataBinding.Converters;

internal class WeatherConditionToImageConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        WeatherType weatherType = (WeatherType)value!;

        return weatherType switch
        { 
            WeatherType.Sunny => ImageSource.FromFile("sunny.png"),
            WeatherType.Cloudy => ImageSource.FromFile("cloudy.png"),
            _ => ImageSource.FromFile("question.png")
        };
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture) => throw new NotImplementedException();
}
