﻿namespace Learn3_DataBinding.Models;

public enum WeatherType
{
    Sunny,
    Cloudy
}
