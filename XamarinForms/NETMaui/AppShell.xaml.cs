﻿using System;
using System.Collections.Generic;
using XamarinForms.ViewModels;
using XamarinForms.Views;
using Microsoft.Maui.Controls;
using Microsoft.Maui;

namespace XamarinForms
{
    public partial class AppShell : Microsoft.Maui.Controls.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
