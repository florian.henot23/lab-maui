using Shiny.BluetoothLE.Hosting;
using Shiny;
using System.Diagnostics;
using System.Text;

namespace Bluetooth;

public partial class Server : ContentPage
{
	readonly IBleHostingManager _hostingManager;
	IGattCharacteristic? _notifyCharacteristic;

	public Server(IBleHostingManager hostingManager)
	{
		_hostingManager = hostingManager;

		InitializeComponent();

        swAdvert.Toggled += SwAdvert_Toggled;
        btnWrite.Clicked += BtnWrite_Clicked;

		WriteCharacteristic.OnWriteReceived += OnWriteReceived;
	}

    protected override void OnDisappearing()
    {
		swAdvert.IsToggled = false;
    }

    private void BtnWrite_Clicked(object? sender, EventArgs e)
    {
        var data = Encoding.UTF8.GetBytes(edtWrite.Text);

		WriteAndNotify(data);
    }

    private async void SwAdvert_Toggled(object? sender, ToggledEventArgs e)
    {
        if (e.Value)
		{
			if (!await CheckBtSettings())
            {
                await Shell.Current.GoToAsync("//MainPage");
                return;
            }

            await BeginAdvertising();

			btnWrite.IsEnabled = true;
			edtWrite.IsEnabled = true;
		}
        else
        {
            btnWrite.IsEnabled = false;
            edtWrite.IsEnabled = false;

			StopAdvertising();
        }
    }

    async Task<bool> CheckBtSettings()
	{
        var access = await _hostingManager.RequestAccess(connect: false);

        if (access != Shiny.AccessState.Available)
        {
            await DisplayAlert("Erreur", "Bluetooth non disponible", "Ok");
            Debug.Print("BT AccessState : {0}", access);

            return false;
        }

        return true;
    }

	async Task BeginAdvertising()
	{
		if (_hostingManager.IsAdvertising) return;

		if (!_hostingManager.IsRegisteredServicesAttached)
            await _hostingManager.AttachRegisteredServices();

        _notifyCharacteristic = _hostingManager.Services[0].Characteristics
			.First(c => c.Uuid == MauiProgram.NotifyCharacteristicUuid);

        await _hostingManager.StartAdvertising(new AdvertisementOptions
        {
            LocalName = "MauiServer",
            ServiceUuids = [MauiProgram.ServiceUuid]
        });
    }

	void OnWriteReceived(byte[] data)
	{
		lblRead.Text = Encoding.UTF8.GetString(data);
	}

	void WriteAndNotify(byte[] data)
	{
		ReadCharacteristic.ValueToSend = data;
		_notifyCharacteristic?.Notify(data, [.. NotifyCharacteristic.Peripherals]);
	}

	void StopAdvertising()
	{
        if (!_hostingManager.IsAdvertising) return;

        //if (!_hostingManager.IsRegisteredServicesAttached) return;

        _hostingManager.StopAdvertising();

        _notifyCharacteristic = null;

        //_hostingManager.DetachRegisteredServices();
    }
}