﻿using Shiny.BluetoothLE.Hosting;
using Shiny.BluetoothLE.Hosting.Managed;
using System.Text;

namespace Bluetooth;

[BleGattCharacteristic(MauiProgram.ServiceUuid, MauiProgram.WriteCharacteristicUuid, Write = WriteOptions.WriteWithoutResponse)]
class WriteCharacteristic : BleGattCharacteristic
{
    public delegate void WriteReceived(byte[] data);

    public static WriteReceived? OnWriteReceived { get; set; }

    public override Task OnWrite(WriteRequest request)
    {
        //OnWriteReceived?.Invoke(request.Data);

        var test = request.Data;

        return Task.CompletedTask;
    }
}
