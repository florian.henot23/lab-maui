﻿using Microsoft.Extensions.Logging;
using Shiny;

namespace Bluetooth
{
    public static class MauiProgram
    {
        public const string ServiceUuid = "83a59a91-06c8-4e8d-abcf-a953fabb256b";
        //public const string ServiceUuid = "0000ffe0-0000-1000-8000-00805F9B34FB";
        public const string WriteCharacteristicUuid = "83a59a92-06c8-4e8d-abcf-a953fabb256b";
        //public const string WriteCharacteristicUuid = "0000ffe1-0000-1000-8000-00805F9B34FB";
        public const string ReadCharacteristicUuid = "83a59a93-06c8-4e8d-abcf-a953fabb256b";
        //public const string ReadCharacteristicUuid = "0000ffe1-0000-1000-8000-00805F9B34FB";
        public const string NotifyCharacteristicUuid = "83a59a94-06c8-4e8d-abcf-a953fabb256b";
        //public const string NotifyCharacteristicUuid = "0000ffe1-0000-1000-8000-00805F9B34FB";

        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseShiny()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
    		builder.Logging.AddDebug();
#endif

            builder.Services.AddBluetoothLE();
            builder.Services.AddSingleton<Client>();

#if ANDROID || IOS || MACCATALYST
            //builder.Services.AddBluetoothLeHosting();
            builder.Services.AddBleHostedCharacteristic<ReadCharacteristic>();
            builder.Services.AddBleHostedCharacteristic<WriteCharacteristic>();
            builder.Services.AddBleHostedCharacteristic<NotifyCharacteristic>();
#endif

            builder.Services.AddSingleton<Server>();

            return builder.Build();
        }
    }
}
