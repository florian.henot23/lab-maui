﻿using Shiny.BluetoothLE.Hosting;
using Shiny.BluetoothLE.Hosting.Managed;

namespace Bluetooth;

[BleGattCharacteristic(MauiProgram.ServiceUuid, MauiProgram.ReadCharacteristicUuid)]
class ReadCharacteristic : BleGattCharacteristic
{
    public static byte[]? ValueToSend { get; set; }

    public override Task<GattResult> OnRead(ReadRequest request)
    {
        return Task.FromResult(GattResult.Success(ValueToSend ?? []));
    }
}
