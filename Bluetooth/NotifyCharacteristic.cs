﻿using Shiny.BluetoothLE.Hosting;
using Shiny.BluetoothLE.Hosting.Managed;

namespace Bluetooth;

[BleGattCharacteristic(MauiProgram.ServiceUuid, MauiProgram.NotifyCharacteristicUuid, Notifications = NotificationOptions.Notify | NotificationOptions.Indicate)]
class NotifyCharacteristic : BleGattCharacteristic
{
    public static List<IPeripheral> Peripherals { get; }

    static NotifyCharacteristic()
    {
        Peripherals = [];
    }

    public override void OnStop()
    {
        Peripherals.Clear();
    }

    public override Task OnSubscriptionChanged(IPeripheral peripheral, bool subscribed)
    {
        if (subscribed)
            Peripherals.Add(peripheral);
        else
            Peripherals.Remove(peripheral);

        return Task.CompletedTask;
    }
}
