using Microsoft.Extensions.Logging;
using Shiny.BluetoothLE;
using System.Diagnostics;
using System.Text;

namespace Bluetooth;

public partial class Client : ContentPage
{
	readonly IBleManager _bleManager;

	IDisposable? _scanSub;
	IDisposable? _notifySub;

	int conPeriphCount = 0;

	public Client(IBleManager bleManager)
	{
		_bleManager = bleManager;

		InitializeComponent();

        swScan.Toggled += SwScan_Toggled;
        btnRead.Clicked += BtnRead_Clicked;
        btnWrite.Clicked += BtnWrite_Clicked;
        swSub.Toggled += SwSub_Toggled;
	}

    private async void SwSub_Toggled(object? sender, ToggledEventArgs e)
    {
		if (e.Value) await Subscribe();
		else Unsubscribe();
    }

    private async void SwScan_Toggled(object? sender, ToggledEventArgs e)
    {
		if (!e.Value)
		{
            StopScan();

            btnRead.IsEnabled = true;
            btnWrite.IsEnabled = true;
            swSub.IsEnabled = true;
            edtWrite.IsEnabled = true;
        }
		else
		{
            if (!await CheckBtSettings())
            {
				await Shell.Current.GoToAsync("//MainPage");
				return;
            }

			ScanAndConnect();
        }
    }

    private async void BtnWrite_Clicked(object? sender, EventArgs e)
    {
        var data = Encoding.UTF8.GetBytes(edtWrite.Text);

		//await Write(data);
		await WriteAndroid(data);
    }

    private async void BtnRead_Clicked(object? sender, EventArgs e)
    {
		var data = await Read();
		if (data is null) return;

		lblRead.Text = Encoding.UTF8.GetString(data);
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();

        lblScan.Text = "En attente de scan";
        lblRead.Text = "...";
		lblSubRead.Text = "...";

		btnRead.IsEnabled = false;
		btnWrite.IsEnabled = false;
		swSub.IsEnabled = false;
		edtWrite.IsEnabled = false;

		conPeriphCount = 0;
    }

    protected override void OnDisappearing()
    {
		base.OnDisappearing();

        swScan.IsToggled = false; //StopScan
		swSub.IsToggled = false; //Unsubscribe

		foreach (var periph in _bleManager.GetConnectedPeripherals())
			periph.CancelConnection();
    }

    async Task<bool> CheckBtSettings()
	{
		var access = await _bleManager.RequestAccessAsync();

		if (access != Shiny.AccessState.Available)
		{
			await DisplayAlert("Erreur", "Bluetooth non disponible", "Ok");
			Debug.Print("BT AccessState : {0}", access);

			return false;
		}

		return true;
	}

	void ScanAndConnect()
	{
		if (_bleManager.IsScanning) return;

		_scanSub = _bleManager.ScanForUniquePeripherals(new ScanConfig(MauiProgram.ServiceUuid))
			.Subscribe(async (peripheral) =>
			{
				await peripheral.ConnectAsync(new ConnectionConfig(false), timeout: TimeSpan.FromSeconds(5));

				MainThread.BeginInvokeOnMainThread(() =>
				{
					lblScan.Text = $"Connect� � {++conPeriphCount} p�riph�rique(s)";
				});

				Debug.Print("BT Connect� au p�riph�rique : {0}", peripheral.Name);
			});
	}

	async Task<byte[]?> Read()
	{
		var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
		if (peripheral is null)
		{
			await DisplayAlert("Erreur", "Aucun appareil connect�. Effectuez un scan", "Ok");
			return null;
		}

		var res = await peripheral.ReadCharacteristicAsync(MauiProgram.ServiceUuid, MauiProgram.ReadCharacteristicUuid);

		if (res is null)
		{
            await DisplayAlert("Erreur", "Impossible de r�cup�rer les donn�es", "Ok");
            return null;
        }

        Debug.Print("BT Donn�es r�cup�r�es");

        return res.Data;
	}

	async Task Write(byte[] data)
	{
        var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
        if (peripheral is null)
        {
            await DisplayAlert("Erreur", "Aucun appareil connect�. Effectuez un scan", "Ok");
            return;
        }

		var res = await peripheral.WriteCharacteristicAsync(MauiProgram.ServiceUuid, MauiProgram.WriteCharacteristicUuid, data, withResponse: false);

		if (res is null)
            await DisplayAlert("Erreur", "Impossible d'envoyer les donn�es", "Ok");

        Debug.Print("BT Donn�es envoy�es");
    }

	async Task WriteAndroid(byte[] data)
	{
        var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
        if (peripheral is null)
        {
            await DisplayAlert("Erreur", "Aucun appareil connect�. Effectuez un scan", "Ok");
            return;
        }

		using var trans = peripheral.TryBeginTransaction() ?? throw new InvalidOperationException("Le p�riph�rique ne prend pas en charge les transactions.");

		trans.Write(peripheral, MauiProgram.ServiceUuid, MauiProgram.WriteCharacteristicUuid, data);

		trans.Commit();

        Debug.Print("BT Donn�es envoy�es");
    }

    async Task Subscribe()
	{
		if (_notifySub is not null) return;

        var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
        if (peripheral is null)
        {
            await DisplayAlert("Erreur", "Aucun appareil connect�. Effectuez un scan", "Ok");
            return;
        }

		_notifySub = peripheral.NotifyCharacteristic(MauiProgram.ServiceUuid, MauiProgram.NotifyCharacteristicUuid)
			.Subscribe(res =>
			{
				MainThread.BeginInvokeOnMainThread(() =>
				{
                    lblSubRead.Text = Encoding.UTF8.GetString(res.Data!);
                });

                Debug.Print("BT Notification - Donn�es re�ues");
            });
    }

	void StopScan()
	{
        _scanSub?.Dispose();
        _scanSub = null;
    }

	void Unsubscribe()
	{
        _notifySub?.Dispose();
        _notifySub = null;
    }
}