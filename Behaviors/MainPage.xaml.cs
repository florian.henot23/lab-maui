﻿using Behaviors.Behaviors;

namespace Behaviors
{
    public partial class MainPage : ContentPage
    {
        int count = 0;

        public MainPage()
        {
            InitializeComponent();
            PushPageBtn.Clicked += PushPageBtn_Clicked;
        }

        private void PushPageBtn_Clicked(object? sender, EventArgs e)
        {
            var toRemove = CounterBtn.Behaviors.FirstOrDefault(b => b is TextSizeButtonBorderBehavior);
            if (toRemove is not null) CounterBtn.Behaviors.Remove(toRemove);

            Navigation.PushAsync(new ContentPage());
        }

        private void OnCounterClicked(object sender, EventArgs e)
        {
            count++;

            if (count == 1)
                CounterBtn.Text = $"Clicked {count} time";
            else
                CounterBtn.Text = $"Clicked {count} times";

            SemanticScreenReader.Announce(CounterBtn.Text);
        }
    }
}
