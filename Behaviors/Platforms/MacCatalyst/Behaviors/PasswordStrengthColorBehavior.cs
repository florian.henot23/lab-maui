﻿using UIKit;

namespace Behaviors.Behaviors;

partial class PasswordStrengthColorBehavior : PlatformBehavior<Entry, UIView>
{
    // NO-OP on MacCatalyst
}
