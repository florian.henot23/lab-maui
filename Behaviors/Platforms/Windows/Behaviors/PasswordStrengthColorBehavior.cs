﻿using Microsoft.UI.Xaml;

namespace Behaviors.Behaviors;

partial class PasswordStrengthColorBehavior : PlatformBehavior<Entry, FrameworkElement>
{
    // NO-OP on Windows
}
