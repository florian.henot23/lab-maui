﻿using UIKit;
using Microsoft.Maui.Platform;

namespace Behaviors.Behaviors;

partial class PasswordStrengthColorBehavior : PlatformBehavior<Entry, UITextField>
{
    protected override void OnAttachedTo(Entry bindable, UITextField platformView)
    {
        base.OnAttachedTo(bindable, platformView);

        if (bindable is null) return;

        platformView.Ended += PlatformView_Ended;
    }

    void PlatformView_Ended(object? sender, EventArgs e)
    {
        var textField = (UITextField)sender!;

        textField.TintColor = textField.Text?.Length switch
        {
            >= 16 and < 40 => MediumColor.ToPlatform(),
            >= 40 => StrongColor.ToPlatform(),
            _ => WeakColor.ToPlatform()
        };
    }

    protected override void OnDetachedFrom(Entry bindable, UITextField platformView)
    {
        base.OnDetachedFrom(bindable, platformView);

        if (bindable is null) return;

        platformView.Ended -= PlatformView_Ended;
    }
}
