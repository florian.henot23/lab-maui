﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

class TextSizeButtonBorderBehavior : Behavior<Button>
{
    public static readonly BindableProperty BorderColorProperty =
        BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(TextSizeButtonBorderBehavior), Colors.Gray);

    public Color BorderColor
    {
        get => (Color)GetValue(BorderColorProperty);
        set => SetValue(BorderColorProperty, value);
    }

    protected override void OnAttachedTo(Button bindable)
    {
        bindable.BorderColor = BorderColor;
        bindable.Clicked += Bindable_Clicked;

        base.OnAttachedTo(bindable);
    }

    void Bindable_Clicked(object? sender, EventArgs e)
    {
        var button = (Button)sender!;

        button.BorderWidth = button.Text.Length - 5;
    }

    protected override void OnDetachingFrom(Button bindable)
    {
        bindable.Clicked -= Bindable_Clicked;

        base.OnDetachingFrom(bindable);
    }
}
