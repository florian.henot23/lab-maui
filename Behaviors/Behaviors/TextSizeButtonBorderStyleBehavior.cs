﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

class TextSizeButtonBorderStyleBehavior : Behavior<Button>
{
    #region AttachBehaviorProperty
    public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(TextSizeButtonBorderStyleBehavior), false, propertyChanged: OnAttachBehaviorChanged);

    public static bool GetAttachBehavior(BindableObject view) => (bool)view.GetValue(AttachBehaviorProperty);

    public static void SetAttachBehavior(BindableObject view, bool value) => view.SetValue(AttachBehaviorProperty, value);
    #endregion

    static void OnAttachBehaviorChanged(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is not Button button) return;

        bool attachBehavior = (bool)newValue;
        if (attachBehavior) button.Behaviors.Add(new TextSizeButtonBorderStyleBehavior());
        else
        {
            var toRemove = button.Behaviors.FirstOrDefault(b => b is TextSizeButtonBorderStyleBehavior);
            if (toRemove is not null) button.Behaviors.Remove(toRemove);
        }
    }

    protected override void OnAttachedTo(Button bindable)
    {
        bindable.BorderColor = Colors.Gray;
        bindable.Clicked += Bindable_Clicked;

        base.OnAttachedTo(bindable);
    }

    void Bindable_Clicked(object? sender, EventArgs e)
    {
        var button = (Button)sender!;

        button.BorderWidth = button.Text.Length - 5;
    }

    protected override void OnDetachingFrom(Button bindable)
    {
        bindable.Clicked -= Bindable_Clicked;

        base.OnDetachingFrom(bindable);
    }
}
