﻿namespace Behaviors.Behaviors;

partial class PasswordStrengthColorBehavior
{
    static readonly Color WeakColor = Colors.Crimson;
    static readonly Color MediumColor = Colors.Coral;

    public static readonly BindableProperty StrongColorProperty =
        BindableProperty.Create(nameof(StrongColor), typeof(Color), typeof(PasswordStrengthColorBehavior), Colors.Green);

    public Color StrongColor
    {
        get => (Color)GetValue(StrongColorProperty);
        set => SetValue(StrongColorProperty, value);
    }
}
