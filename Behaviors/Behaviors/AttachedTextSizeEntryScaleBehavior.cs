﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

/// <summary>
/// Comportement attaché : utilisation de <a href="https://learn.microsoft.com/fr-fr/dotnet/maui/fundamentals/attached-properties?view=net-maui-8.0">propriétés jointes</a>
/// </summary>
static class AttachedTextSizeEntryScaleBehavior
{
    /// <summary>
    /// Propriété jointe obligatoire pour spécifier si le comportement doit être attaché ou non
    /// </summary>
    #region AttachBehaviorProperty
    public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(AttachedTextSizeEntryScaleBehavior), false, propertyChanged: OnAttachBehaviorChanged);

    public static bool GetAttachBehavior(BindableObject view) => (bool)view.GetValue(AttachBehaviorProperty);

    public static void SetAttachBehavior(BindableObject view, bool value) => view.SetValue(AttachBehaviorProperty, value);
    #endregion

    static void OnAttachBehaviorChanged(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is not Entry entry) return;

        /* On peut soit :
         * - affecter une propriété du contrôle directement
         * - affecter une propriété du contrôle en fonction d'un évènement du contrôle (= trigger)
         * Ici on utilise un gestionnaire qu'on inscrit pour un évènement si le comportement est attaché, et inversement
         */
        bool attachBehavior = (bool)newValue;
        if (attachBehavior) entry.TextChanged += Entry_TextChanged;
        else entry.TextChanged -= Entry_TextChanged;
    }

    static void Entry_TextChanged(object? sender, TextChangedEventArgs e)
    {
        var entry = (Entry)sender!;

        entry.Scale = 0.7 + (entry.Text.Length * 0.1);
    }
}
